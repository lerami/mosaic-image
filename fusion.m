function [ I ] = fusion( I1,I2 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[ M1, A1, B1 ] = redef_image( I1 ); 
[ M2, A2, B2 ] = redef_image( I2 );

[ tx,ty ] = correlation_phase( I1,I2 );

A2 = [A2(1)+ty,A2(2)+tx];
B2 = [B2(1)+ty,B2(2)+tx];

A3 = min(A1,A2);
B3 = max(B1,B2);

t_boite_masque = A3 - 1;

A1 = A1 - t_boite_masque;
B1 = B1 - t_boite_masque;
A2 = A2 - t_boite_masque;
B2 = B2 - t_boite_masque;

% récupération extrêmes et masque
M1 = ones(S1);
M2 = ones(S2);

% taille nv masque = max taille des 2 masques +translation
TMx = max(w1,w2);
TMy = max(h1,h2);
M = zeros(TMx + tx , TMy + ty);


end

