function [A] = fusion2(I1,M1,B1,I2,M2,B2)

%% Déterminer la position des images
S1 = size(I1)
S2 = size(I2)

Py = min(B1(1),B2(1))
Px = min(B1(2),B2(2))


%% Déterminer la taille de la boite
Ty = max(B1(1),B2(1)) + S1(1)
Tx = max(B1(2),B2(2)) + S1(2)

Sy = Ty - Py 
Sx = Tx - Px
 

A = zeros(Sy,Sx);


%% Remplir le nouveau masque
New_B1 = [B1(1)-Py+1,B1(2)-Px+1]
New_B2 = [B2(1)-Py+1,B2(2)-Px+1]

M = zeros(Sy,Sx);
M(New_B1(1):New_B1(1)+S1(1)-1,New_B1(2):New_B1(2)+S1(2)-1) = M1;
M(New_B2(1):New_B2(1)+S2(1)-1,New_B2(2):New_B2(2)+S2(2)-1) = M(New_B2(1):New_B2(1)+S2(1)-1,New_B2(2):New_B2(2)+S2(2)-1) + M2;

%%% Déterminer la superposition des intensités
A(New_B1(1):New_B1(1)+S1(1)-1,New_B1(2):New_B1(2)+S1(2)-1) = I1; 
A(New_B2(1):New_B2(1)+S2(1)-1,New_B2(2):New_B2(2)+S2(2)-1) = A(New_B2(1):New_B2(1)+S2(1)-1,New_B2(2):New_B2(2)+S2(2)-1) + I2;


%% Créer l'image finale
A = A./M;


end

