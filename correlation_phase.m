function [ tx,ty ] = correlation_phase( I1,I2 )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[h,w] = size(I1);

% Calculs TF
TF1 = fft2(I1);

TF2 = fft2(I2);

% Calcul spectre de puissance croisée
P = (TF2.*conj(TF1))./abs(TF2.*conj(TF1));


% calcul TF inverse
p = ifft2(P);
%figure, surf(p);

% get max value indexes
maxValue = max(p(:));
[ty,tx] = find(p == maxValue);

if (ty < h/2-1)
    ty = -ty +1;
else
    ty = h-ty+1;
end

if (tx < w/2+1)
    tx = -tx +1;
else
    tx = w-tx+1;
end


end

