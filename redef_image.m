function [ M,A,B ] = redef_image( I )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

S = size(I);
h = S(1);
w = S(2);

A = [-10,-10];
B = [A(1)+h,A(2)+w];

M = ones(size(I));

end

