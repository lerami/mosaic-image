%% projet image

clear; close all; clc;
figure,
imshow(imread('pont.jpg'));
A = double(imread('pont.jpg'));
% Size = [h,w] = [nb_lignes,nb_colonnes] = [y,x] = [i,j]
I = A(:,:,1);
h = 100;
w = 100;
posx = 40;
posy = 40;


% dans le cas de petites translations : tx < w/2 et ty < h/2
tx = 30;
ty = 30;

[I1,I2] = decoupage( I,w,h,posx,posy,tx,ty );


[ tx,ty ] = correlation_phase( I1,I2 );


%% Fusion

[N1,M1,B1] = traitement(I1);
[N2,M2,B2] = traitement(I2);

B2(1) = B2(1)+ty;
B2(2) = B2(2)+tx;

B = fusion2(N1,M1,B1,N2,M2,B2);

figure,
imshow(uint8(B));

