function [ I1,I2 ] = decoupage( I,w,h,posx,posy,tx,ty )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

S = size(I);
% S(1) = height , S(2) = width

if (posy+h-1 > S(1) || posx+w-1 > S(2) || ty+posy+h-1 > S(1) || posx+tx+w-1 > S(2))
    display('Warning !! Les valeurs de transitions et/ou de tailles sont trop élevées');
else if (posx+tx-1 < 0 || posy+ty-1 < 0 )
        display('le découpage sort de l image');
else    
I1 = I(posy:posy+h-1,posx:posx+w-1);
I2 = I(posy+ty:posy+h+ty-1,posx+tx:posx+w+tx-1);
end

end

